section .text
	global _ft_strdup
	extern _ft_strlen, _ft_strcpy, _malloc

_ft_strdup:
	push rdi
	call _ft_strlen
	mov rdi, rax
	inc rdi
	call _malloc
	pop rsi
	mov rdi, rax
	push rdi
	call _ft_strcpy
	pop rax
	ret
