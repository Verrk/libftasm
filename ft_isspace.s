section .text
	global _ft_isspace

_ft_isspace:
	xor rax, rax
	cmp rdi, 0x09
	jl _exit
	cmp rdi, 0x0D
	jg _sp
	mov rax, 1
	jmp _exit

_sp:
	cmp rdi, 0x20
	jne _exit
	mov rax, 1

_exit:
	ret
