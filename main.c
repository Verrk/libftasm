/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/12 17:56:44 by cpestour          #+#    #+#             */
/*   Updated: 2015/05/14 15:31:36 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include "libfts.h"

#define GREEN "\e[32m"
#define RESET "\033[0m"
#define RED   "\e[31m"

void		test_ft_bzero(void)
{
	char b1[42], b2[42];

	bzero(b1, 10);
	ft_bzero(b2, 10);
	assert(memcmp(b1, b2, 10) == 0);
	bzero(b1, 42);
	ft_bzero(b2, 42);
	assert(memcmp(b1, b2, 42) == 0);
	b1[0] = 1;
	ft_bzero(b1, 0);
	assert(b1[0] == 1);
	printf("ft_bzero:\t[%sOK!%s]\n", GREEN, RESET);
}

void		test_ft_strcat(void)
{
	char buf[9];

	bzero(buf, 9);
	ft_strcat(buf, "Bon");
	ft_strcat(buf, "jour.");
	ft_strcat(buf, "");
	assert(strcmp(buf, "Bonjour.") == 0);
	printf("ft_strcat:\t[%sOK!%s]\n", GREEN, RESET);
}

void		test_ft_isalpha(void)
{
	assert(ft_isalpha('a'));
	assert(ft_isalpha('a' + 0x100) == 0);
	assert(ft_isalpha('2') == 0);
	assert(ft_isalpha('Z'));
	assert(ft_isalpha('t'));
	printf("ft_isalpha:\t[%sOK!%s]\n", GREEN, RESET);
}

void		test_ft_isdigit(void)
{
	assert(ft_isdigit(0) == 0);
	assert(ft_isdigit('0'));
	assert(ft_isdigit('8' + 0x100) == 0);
	printf("ft_isdigit:\t[%sOK!%s]\n", GREEN, RESET);
}

void		test_ft_isalnum(void)
{
	assert(ft_isalnum(' ') == 0);
	assert(ft_isalnum('6'));
	assert(ft_isalnum('Z'));
	assert(ft_isalnum('5' + 0x100) == 0);
	printf("ft_isalnum:\t[%sOK!%s]\n", GREEN, RESET);
}

void		test_ft_isascii(void)
{
	assert(ft_isascii(200) == 0);
	assert(ft_isascii(0));
	assert(ft_isascii(127));
	printf("ft_isascii:\t[%sOK!%s]\n", GREEN, RESET);
}

void		test_ft_isprint(void)
{
	assert(ft_isprint(0) == 0);
	assert(ft_isprint(127) == 0);
	assert(ft_isprint(' '));
	assert(ft_isprint('a'));
	assert(ft_isprint('~'));
	printf("ft_isprint:\t[%sOK!%s]\n", GREEN, RESET);
}

void		test_ft_toupper(void)
{
	assert(ft_toupper('a') == 'A');
	assert(ft_toupper(12345) == 12345);
	assert(ft_toupper(' ') == ' ');
	assert(ft_toupper('Z') == 'Z');
	assert(ft_toupper('z') == 'Z');
	printf("ft_toupper:\t[%sOK!%s]\n", GREEN, RESET);
}

void		test_ft_tolower(void)
{
	assert(ft_tolower('T') == 't');
	assert(ft_tolower(99999) == 99999);
	assert(ft_tolower('e') == 'e');
	assert(ft_tolower('Z') == 'z');
	printf("ft_tolower:\t[%sOK!%s]\n", GREEN, RESET);
}

void		test_ft_puts(void)
{
	int		p[2];
	char	buf[4096];
	int		n;
	int stdout_cpy = dup(1);

	pipe(p);
	dup2(p[1], 1);
	ft_puts("aa");
	ft_puts(NULL);
	n = read(p[0], buf, 4095);
	buf[n] = '\0';
	assert(strcmp(buf, "aa\n(null)\n") == 0);
	assert(ft_puts("toto") >= 0);
	close(1);
	assert(ft_puts("hello") == -1);
	dup2(stdout_cpy, 1);
	printf("ft_puts:\t[%sOK!%s]\n", GREEN, RESET);
}

void		test_ft_strlen(void)
{
	assert(ft_strlen("chat") == 4);
	assert(ft_strlen("") == 0);
	assert(ft_strlen("abcde") == 5);
	printf("ft_strlen:\t[%sOK!%s]\n", GREEN, RESET);
}

void		test_ft_memset(void)
{
	char	b1[100], b2[100];

	ft_memset(b1, 42, 100);
	memset(b2, 42, 100);
	assert(memset(b1, 99, 0) == ft_memset(b1, 99, 0));
	assert(memcmp(b1, b2, 100) == 0);
	b1[0] = 1;
	ft_memset(b1, 0, 0);
	assert(b1[0] == 1);
	printf("ft_memset:\t[%sOK!%s]\n", GREEN, RESET);
}

void		test_ft_memcpy(void)
{
	char	b1[10];
	char	b2[10] = {7, 72, 68, -44, 0, 6, 8, 9, 0, 77};

	ft_memcpy(b1, b2, 10);
	assert(memcmp(b1, b2, 10) == 0);
	assert(b1 == ft_memcpy(b1, b2, 0));
	printf("ft_memcpy:\t[%sOK!%s]\n", GREEN, RESET);
}

void		test_ft_strdup(void)
{
	char	*str = "AbC";

	assert(strcmp(ft_strdup("aaaaa"), "aaaaa") == 0);
	assert(strcmp(ft_strdup(""), "") == 0);
	assert(str != ft_strdup(str));
	assert(ft_strdup("abc")[4] == 0);
	printf("ft_strdup:\t[%sOK!%s]\n", GREEN, RESET);
}

void		test_ft_cat(void)
{
	int		p[2];
	int		d[2];
	const char	data[] = "egiuweugfw\negwfuegufgue\n\n\newuewehufhufhuwefuhiwehiuwe\nefwefygwugfygyuweygugwegyfgyfwe\n\neijrgeigi";
	char	buf[4096];
	int		c;
	int		stdout_cpy = dup(1);

	pipe(p);
	pipe(d);
	write(d[1], data, sizeof(data));
	close(d[1]);
	dup2(p[1], 1);
	ft_cat(d[0]);
	c = read(p[0], buf, 4096);
	buf[c] = '\0';
	close(p[0]);
	close(p[1]);
	close(d[0]);
	assert(strcmp(buf, data) == 0);
	dup2(stdout_cpy, 1);
	printf("ft_cat:\t\t[%sOK!%s]\n", GREEN, RESET);
}

void		test_ft_islower(void)
{
	assert(ft_islower('a'));
	assert(ft_islower('A') == 0);
	assert(ft_islower(200) == 0);
	assert(ft_islower('a' + 0x100) == 0);
	assert(ft_islower('z'));
	printf("ft_islower:\t[%sOK!%s]\n", GREEN, RESET);
}

void		test_ft_isupper(void)
{
	assert(ft_isupper('a') == 0);
	assert(ft_isupper('A'));
	assert(ft_isupper(200) == 0);
	assert(ft_isupper('a' + 0x100) == 0);
	assert(ft_isupper('Z'));
	printf("ft_isupper:\t[%sOK!%s]\n", GREEN, RESET);
}

void		test_ft_isspace(void)
{
	assert(ft_isspace(' '));
	assert(ft_isspace('\f'));
	assert(ft_isspace('\n'));
	assert(ft_isspace('\r'));
	assert(ft_isspace('\t'));
	assert(ft_isspace('\v'));
	assert(ft_isspace('a') == 0);
	printf("ft_isspace:\t[%sOK!%s]\n", GREEN, RESET);
}

void		test_ft_strcmp(void)
{
	assert(ft_strcmp("a", "a") == 0);
	assert(ft_strcmp("ab", "a"));
	assert(ft_strcmp("", "a"));
	assert(ft_strcmp("a", "b") == -1);
	assert(ft_strcmp("abcde", "abdcde"));
	printf("ft_strcmp:\t[%sOK!%s]\n", GREEN, RESET);
}

void		test_ft_strcpy(void)
{
	char	buf[9];

	bzero(buf, 9);
	ft_strcpy(buf, "Bonjour.");
	assert(strcmp(buf, "Bonjour.") == 0);
	ft_strcpy(buf, "a");
	assert(strcmp(buf, "a") == 0);
	printf("ft_strcpy:\t[%sOK!%s]\n", GREEN, RESET);
}

int			main()
{
	printf("%s--- PART1 ---%s\n", RED, RESET);
	test_ft_bzero();
	test_ft_strcat();
	test_ft_isalpha();
	test_ft_isdigit();
	test_ft_isalnum();
	test_ft_isascii();
	test_ft_isprint();
	test_ft_toupper();
	test_ft_tolower();
	test_ft_puts();
	printf("%s--- PART2 ---%s\n", RED, RESET);
	test_ft_strlen();
	test_ft_memset();
	test_ft_memcpy();
	test_ft_strdup();
	printf("%s--- CAT ---%s\n", RED, RESET);
	test_ft_cat();
	printf("%s--- BONUS ---%s\n", RED, RESET);
	test_ft_islower();
	test_ft_isupper();
	test_ft_isspace();
	test_ft_strcmp();
	test_ft_strcpy();
}
