%define WRITE 0x2000004
%define READ 0x2000003
%define STDOUT 1

section .bss
buf:	resb 0x400

section .text
	global _ft_cat
	extern _ft_bzero

_ft_cat:
	push rdi
	lea rdi, [rel buf]
	mov rsi, 0x400
	call _ft_bzero
	pop rdi
	lea rsi, [rel buf]
	mov rdx, 0x400
	mov rax, READ
	syscall
	jc	_err
	cmp rax, 0
	je _exit
	push rdi
	mov rdi, STDOUT
	mov rdx, rax
	mov rax, WRITE
	syscall
	jc	_err
	pop rdi
	jmp _ft_cat

_exit:
	mov rax, 0
	ret

_err:
	mov rax, -1
	ret
