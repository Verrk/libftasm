section	.text
	global	_ft_strcat

_ft_strcat:
	push rdi
	cld

_end_s1:
	cmp BYTE[rdi], 0x0
	je _concat
	inc rdi
	jmp _end_s1

_concat:
	cmp	byte[rsi], 0x0
	je	_exit
	movsb
	jmp	_concat

_exit:
	pop rax
	ret
