section	.text
	global	_ft_isalnum
	extern	_ft_isalpha
	extern	_ft_isdigit

_ft_isalnum:
	xor	rax, rax
	call	_ft_isalpha
	cmp	rax, 1
	je	_exit
	call	_ft_isdigit

_exit:
	ret
