section	.text
	global	_ft_isalpha
	extern	_ft_islower
	extern	_ft_isupper

_ft_isalpha:
	xor	rax, rax
	call	_ft_islower
	cmp	rax, 1
	je	_exit
	call	_ft_isupper

_exit:
	ret
