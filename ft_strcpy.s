section .text
	global _ft_strcpy
	extern _ft_strlen

_ft_strcpy:
	push rdi
	mov rdi, rsi
	call _ft_strlen
	mov rcx, rax
	inc rcx
	pop rdi
	push rdi
	rep movsb
	pop rax
	ret