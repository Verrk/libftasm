section	.text
	global	_ft_strlen

_ft_strlen:
	xor	eax, eax
	cmp	rdi, 0
	jz	_exit
	xor	ecx, ecx
	dec	ecx
	cld
	repne	scasb
	not	ecx
	dec	ecx
	mov	eax, ecx

_exit:
	ret
