section	.text
	global	_ft_isprint

_ft_isprint:
	xor	rax, rax
	cmp rdi, 32
	jl	_exit
	cmp	rdi, 126
	jg	_exit
	mov	rax, 1

_exit:
	ret
