section	.text
	global	_ft_toupper
	extern	_ft_islower

_ft_toupper:
	call	_ft_islower
	cmp	rax, 0
	je	_exit
	sub	rdi, 32

_exit:
	mov	rax, rdi
	ret
