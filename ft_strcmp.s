section .text
	global _ft_strcmp

_ft_strcmp:
	cld
	cmp BYTE[rdi], 0
	je _exit
	cmpsb
	jne _mismatch
	jmp _ft_strcmp

_mismatch:
	dec rsi
	dec rdi

_exit:
	xor rax, rax
	mov al, BYTE[rdi]
	sub al, BYTE[rsi]
	cmp rax, 127
	jg _neg
	ret

_neg:
	mov rbx, 0xffffff00
	or rax, rbx
	ret