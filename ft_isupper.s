section	.text
	global	_ft_isupper

_ft_isupper:
	xor	rax, rax
	cmp	rdi, 65
	jl	_exit
	cmp	rdi, 90
	jg	_exit
	mov	rax, 1

_exit:
	ret
