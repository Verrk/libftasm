%define WRITE 0x2000004
%define STDOUT 0x1

global	_ft_puts
extern	_ft_strlen

section .data
null:
	.string db "(null)"

section	.text

_ft_puts:
	cmp rdi, 0x0
	je _null
	push rdi
	call	_ft_strlen
	mov	rdx, rax
	pop rsi
	mov	rdi, STDOUT
	mov	rax, WRITE
	syscall
	jc	_err
	jmp _nl

_null:
	lea rsi, [rel null.string]
	mov rdi, STDOUT
	mov rdx, 6
	mov rax, WRITE
	syscall
	jc	_err

_nl:
	push 0xa
	mov rsi, rsp
	mov rdi, STDOUT
	mov rdx, 1
	mov	rax, WRITE
	syscall
	jc	_err
	pop rax
	ret

_err:
	mov rax, -1
	ret
