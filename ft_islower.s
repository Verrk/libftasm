section	.text
	global	_ft_islower

_ft_islower:
	xor	rax, rax
	cmp	rdi, 97
	jl	_exit
	cmp	rdi, 122
	jg	_exit
	mov	rax, 1

_exit:
	ret
