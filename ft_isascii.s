section	.text
	global	_ft_isascii

_ft_isascii:
	xor	rax, rax
	cmp	rdi, 0
	jl	_exit
	cmp	rdi, 127
	jg	_exit
	mov	rax, 1

_exit:
	ret
