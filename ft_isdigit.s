section	.text
	global	_ft_isdigit

_ft_isdigit:
	xor rax, rax
	cmp	rdi, 48
	jl	_exit
	cmp	rdi, 57
	jg	_exit
	mov	rax, 1

_exit:
	ret
