#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/03/19 02:45:20 by cpestour          #+#    #+#              #
#    Updated: 2015/05/14 14:59:03 by cpestour         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME=libfts.a
NASM=~/.brew/Cellar/nasm/2.11.05/bin/nasm
SRC=ft_bzero.s ft_strlen.s ft_isupper.s ft_islower.s ft_puts.s ft_isalpha.s \
	ft_isdigit.s ft_isalnum.s ft_isascii.s ft_isprint.s ft_toupper.s ft_tolower.s \
	ft_strcat.s ft_cat.s ft_memcpy.s ft_memset.s ft_strcmp.s ft_strdup.s ft_strcpy.s \
	ft_isspace.s
OBJ=$(SRC:.s=.o)

all: $(NAME)

$(NAME): $(OBJ)
	ar rc $@ $^
	ranlib $@

%.o: %.s
	$(NASM) -f macho64 -o $@ $<

clean:
	rm -f *~ *.o *\#

fclean: clean
	rm -f $(NAME) a.out test

re: fclean all

test: $(NAME)
	gcc -o $@ main.c -L. -lfts
	./test

.PHONY: clean, re, test