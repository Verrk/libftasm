section .text
	global	_ft_bzero

_ft_bzero:
	cmp rdi, BYTE 0x0
	jz  _exit

_bzero_loop:
	cmp rsi, 0
	jle _exit
	mov [rdi], BYTE 0x0
	inc rdi
	dec rsi
	jmp _bzero_loop

_exit:
	ret
